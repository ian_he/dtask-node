import {createServiceBusService, QueueMessage, ReceiveQueueMessage} from "azure-sb";
import Bluebird = require("bluebird");
import {ServiceBusServiceAsync} from "../lib/impl/azure";
import {wrapLoop} from "../lib/commonUtil";

const QUEUE_NAME='dtask-node.unittest';

describe('azure queue', () => {
    before(()=>{
        console.log('before');
    });

    // it('should import JobCollection', () => {
    //     console.log('start');
    //     const {testttt} = require('../lib/impl/azureJobCollection');
    //     testttt();
    // });

    it('should delay receive', function(){
        this.timeout(60*1000);
        const _queueService = createServiceBusService('Endpoint=sb://coininvestor.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=Fwg3RtVRrEXsGN4G6y0jAuIjFmezkOXeICqw5/cdGdg=');
        const queueService = Bluebird.promisifyAll(_queueService) as ServiceBusServiceAsync;
        queueService.createQueueIfNotExistsAsync(QUEUE_NAME);
        // return queueService.receiveQueueMessageAsync(QUEUE_NAME)
        const now = new Date();
        console.log('start: ',now.getTime());
        const ScheduledEnqueueTimeUtc = new Date(now.getTime()+ 10 * 1000).toUTCString();
        const message: QueueMessage = {body: 'unit-test', customProperties: {test: true}, brokerProperties: {ScheduledEnqueueTimeUtc}};
        queueService.sendQueueMessageAsync(QUEUE_NAME, message);
        return wrapLoop(()=>{return queueService.receiveQueueMessageAsync(QUEUE_NAME).catch(e=>undefined)},[], (res:ReceiveQueueMessage)=>{
            if(!!res) {
                console.log('get: ', new Date().getTime());
                return true;
            }else
                return false;
        });
    })

});