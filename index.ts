export * from "./lib/JobCollection";
export * from "./lib/Job";
export * from "./lib/Worker";
import * as Azure from "./lib/impl/azure";
export {Azure};
import Bluebird = require("bluebird");
Bluebird.config({
    warnings: {
        wForgottenReturn: false
    }
});