"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./lib/JobCollection"));
__export(require("./lib/Job"));
__export(require("./lib/Worker"));
const Azure = require("./lib/impl/azure");
exports.Azure = Azure;
