export declare type JobId = string | number;
export declare enum JobResultStatus {
    init = "init",
    success = "success",
    fail = "fail",
}
export declare class JobExecResult {
    status: JobResultStatus;
    data: any;
    error: Error;
    runAt: Date;
    completeAt: Date;
    scheduleAt: Date;
    constructor(scheduleAt: Date);
    success(data?: any): void;
    fail(data?: any, err?: Error): void;
}
export interface JobIntf {
    id?: JobId;
    taskId: JobId;
    data: object;
    type: string;
    schedule: ScheduleConfig;
    runs?: JobExecResult[];
    nextRun?: Date;
    status: JobStatus;
    repeats: number;
    retries: number;
}
export declare class ScheduleConfig {
    repeatOpt?: RepeatConfig;
    retryOpt?: RetryConfig;
    after?: Date;
    constructor();
    setAfter(date: Date): this;
    retry({retries, until, wait}: RetryConfig): this;
    repeat({repeats, until, wait}: RepeatConfig): this;
}
export interface RepeatConfig {
    repeats: number;
    until: Date;
    wait: number;
}
export interface RetryConfig {
    retries: number;
    until: Date;
    wait: number;
}
export declare enum JobEvents {
    COMPLETE = "job_done",
    RUNNING = "job_running",
}
export declare enum JobStatus {
    waiting = "waiting",
    running = "running",
    completed = "completed",
    failed = "failed",
    cancelled = "cancelled",
    init = "init",
}
export declare class JobWrap<D> implements JobIntf {
    constructor(jobInDb: JobIntf, jobMessage?: D);
    jobInDb: any;
    jobMessage: D;
    getRemains(): number;
    id: JobId;
    taskId: JobId;
    data: object;
    type: string;
    schedule: ScheduleConfig;
    runs: JobExecResult[];
    nextRun: Date;
    status: JobStatus;
    repeats: number;
    retries: number;
}
