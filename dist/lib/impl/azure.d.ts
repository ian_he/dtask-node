import { QueueMessage, ReceiveQueueMessage, ReceiveQueueMessageOption } from "azure-sb";
import { JobCollectionIntf, JobQueueIntf } from "../JobCollection";
import { JobExecResult, JobIntf, JobWrap } from "../Job";
import Bluebird = require("bluebird");
export interface ServiceBusServiceAsync {
    createQueueIfNotExistsAsync: (queue: string, option?: any) => Bluebird<any>;
    sendQueueMessageAsync: (queuePath: string, message: QueueMessage) => Bluebird<any>;
    receiveQueueMessageAsync: (queuePath: string, options?: ReceiveQueueMessageOption) => Bluebird<ReceiveQueueMessage>;
    deleteMessageAsync: (message: string | QueueMessage) => Bluebird<any>;
}
export declare class AzureQueueAdaptor implements JobQueueIntf<ReceiveQueueMessage> {
    queueService: ServiceBusServiceAsync;
    queueInitLog: Map<string, boolean>;
    constructor();
    onInit(jc: JobCollectionIntf): void;
    beforeSubmit(jc: JobCollectionIntf, job: JobIntf): Promise<void>;
    sendTask(jc: JobCollectionIntf, job: JobWrap<ReceiveQueueMessage>): Promise<void>;
    onComplete(job: JobWrap<ReceiveQueueMessage>, result: JobExecResult): Promise<void>;
    beforeStart(job: JobWrap<any>): void;
    pull(jobType: string, jc: JobCollectionIntf): Promise<ReceiveQueueMessage>;
    getJobId(message: ReceiveQueueMessage): string;
    cancelTask(message: ReceiveQueueMessage): Promise<any>;
    delayJob(job: JobWrap<ReceiveQueueMessage>, delayMills: number): void;
}
