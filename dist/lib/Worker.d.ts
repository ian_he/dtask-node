/// <reference types="node" />
import { JobExecResult, JobIntf } from "./Job";
import { EventEmitter } from 'events';
import Bluebird = require("bluebird");
export declare type Executor = (job: JobIntf, res: JobExecResult) => void | Bluebird<void>;
export declare class WorkerPool {
    workers: Worker[];
    jobBuffer: JobIntf[];
    option: WorkerPoolOption;
    constructor(option: WorkerPoolOption, executor: Executor, eventEmitter: EventEmitter);
    appendJobs(jobs: JobIntf[]): void;
    idle(): boolean;
    readonly prefetch: number;
}
export interface WorkerPoolOption {
    concurrency: number;
    prefetch: number;
}
export declare enum WorkerStatus {
    IDLE = "idle",
    RUNNING = "running",
}
export declare class Worker {
    executor: Executor;
    status: WorkerStatus;
    jobBuffer: JobIntf[];
    eventEmitter: EventEmitter;
    constructor(executor: Executor, jobBuffer: JobIntf[], eventEmitter: EventEmitter);
    run(): Bluebird<any>;
}
