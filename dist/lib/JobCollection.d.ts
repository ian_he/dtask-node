/// <reference types="node" />
import { JobExecResult, JobId, JobIntf, JobWrap, ScheduleConfig } from "./Job";
import { Executor, WorkerPool, WorkerPoolOption } from "./Worker";
import { EventEmitter } from 'events';
import Bluebird = require("bluebird");
export interface JobCollectionIntf {
    config: JobCollectionConfigIntf;
    getQueueName(type: string): string;
    submit(type: string, data: Object, scheduleOption: ScheduleConfig): Promise<JobIntf>;
    reSubmit(job: JobWrap<any>): Promise<JobIntf>;
    cancel(jobId: JobId): void;
    listen(jobType: string, option: WorkerPoolOption, executor: Executor): void;
    find(jobId: JobId): Promise<JobIntf>;
}
export interface JobCollectionConfigIntf {
    queueName: string | ((jobType: string, jc: JobCollectionConfigIntf) => string);
    queuePrefix: string;
    storeName: string | ((_: JobCollectionConfigIntf) => string);
}
export interface JobStoreIntf<T extends JobIntf> {
    onInit: (jc: JobCollectionIntf) => void | Promise<void>;
    beforeSubmit: (jc: JobCollectionIntf, job: JobIntf) => void | Promise<void>;
    onComplete: (job: JobWrap<any>, result: JobExecResult) => void | Promise<void>;
    beforeStart: (job: JobWrap<any>) => void | Promise<void>;
    findById: (id: JobId) => Promise<T>;
    save: (job: T) => void;
}
export interface JobQueueIntf<D> {
    onInit: (jc: JobCollectionIntf) => void | Promise<void>;
    beforeSubmit: (jc: JobCollectionIntf, job: JobIntf) => void | Promise<void>;
    onComplete: (job: JobWrap<D>, result: JobExecResult) => void | Promise<void>;
    beforeStart: (job: JobWrap<D>) => void | Promise<void>;
    sendTask: (jc: JobCollectionIntf, job: JobWrap<D>) => void | Promise<void>;
    pull: (jobType: string, jc: JobCollectionIntf) => Promise<D>;
    getJobId: (msg: D) => JobId;
    cancelTask: (msg: D) => void | Promise<void>;
    delayJob: (job: JobWrap<D>, delayMills: number) => void | Promise<void>;
}
export declare class JobCollection<D> implements JobCollectionIntf {
    interval: number;
    /**
     * key: job_type
     * value: workerpool
     */
    workerPoolMap: Map<string, WorkerPool>;
    eventEmitter: EventEmitter;
    storeAdaptor: JobStoreIntf<JobIntf>;
    queueAdaptor: JobQueueIntf<D>;
    config: JobCollectionConfigIntf;
    constructor(storeAdaptor: JobStoreIntf<JobIntf>, queueAdaptor: JobQueueIntf<D>, config: JobCollectionConfigIntf);
    getQueueName(type: string): string;
    fetchFullJobAndDecide(message: D): Promise<JobWrap<D>>;
    /**
     * TODO: job pull control
     * @return {Bluebird<void>}
     */
    loop(): Bluebird<void>;
    onComplete(job: JobWrap<D>, result: JobExecResult): void;
    onJobStart(job: JobWrap<D>): void;
    submit(type: string, data: Object, scheduleOption: ScheduleConfig): Promise<JobWrap<D>>;
    reSubmit(job: JobWrap<D>): Promise<JobWrap<D>>;
    calcNextRun(job: JobWrap<D>, lastRun?: Date): void;
    cancel(jobId: JobId): void;
    find(jobId: JobId): Promise<JobIntf>;
    listen(jobType: string, option: WorkerPoolOption, executor: Executor): void;
}
