module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/classCallCheck");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/createClass");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("bluebird");

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(1);

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = __webpack_require__(10);
var JobResultStatus;
(function (JobResultStatus) {
    JobResultStatus["init"] = "init";
    JobResultStatus["success"] = "success";
    JobResultStatus["fail"] = "fail";
})(JobResultStatus = exports.JobResultStatus || (exports.JobResultStatus = {}));

var JobExecResult = function () {
    function JobExecResult(scheduleAt) {
        (0, _classCallCheck3.default)(this, JobExecResult);

        this.status = JobResultStatus.init;
        this.runAt = new Date();
        this.scheduleAt = scheduleAt;
    }

    (0, _createClass3.default)(JobExecResult, [{
        key: "success",
        value: function success(data) {
            this.status = JobResultStatus.success;
            this.data = data;
            this.completeAt = new Date();
        }
    }, {
        key: "fail",
        value: function fail(data, err) {
            this.status = JobResultStatus.fail;
            this.data = data;
            this.error = err;
            this.completeAt = new Date();
        }
    }]);
    return JobExecResult;
}();

exports.JobExecResult = JobExecResult;
var DEFAULT_REPEAT = {
    repeats: 0, until: constants_1.FOREVER_DATE, wait: 300000
};
var DEFAULT_RETRY = {
    retries: 0, until: constants_1.FOREVER_DATE, wait: 300000
};

var ScheduleConfig = function () {
    function ScheduleConfig() {
        (0, _classCallCheck3.default)(this, ScheduleConfig);

        this.repeatOpt = Object.assign({}, DEFAULT_REPEAT);
        this.retryOpt = Object.assign({}, DEFAULT_RETRY);
    }

    (0, _createClass3.default)(ScheduleConfig, [{
        key: "setAfter",
        value: function setAfter(date) {
            this.after = date;
            return this;
        }
    }, {
        key: "retry",
        value: function retry(_ref) {
            var retries = _ref.retries,
                until = _ref.until,
                wait = _ref.wait;

            if (retries !== undefined && retries !== null) {
                this.retryOpt.retries = retries;
            }
            if (until) {
                this.retryOpt.until = until;
            }
            if (wait !== undefined && wait !== null) {
                this.retryOpt.wait = wait;
            }
            return this;
        }
    }, {
        key: "repeat",
        value: function repeat(_ref2) {
            var repeats = _ref2.repeats,
                until = _ref2.until,
                wait = _ref2.wait;

            if (repeats !== undefined && repeats !== null) {
                this.repeatOpt.repeats = repeats;
            }
            if (until) {
                this.repeatOpt.until = until;
            }
            if (wait !== undefined && wait !== null) {
                this.repeatOpt.wait = wait;
            }
            return this;
        }
    }]);
    return ScheduleConfig;
}();

exports.ScheduleConfig = ScheduleConfig;
var JobEvents;
(function (JobEvents) {
    JobEvents["COMPLETE"] = "job_done";
    JobEvents["RUNNING"] = "job_running";
})(JobEvents = exports.JobEvents || (exports.JobEvents = {}));
var JobStatus;
(function (JobStatus) {
    JobStatus["waiting"] = "waiting";
    JobStatus["running"] = "running";
    JobStatus["completed"] = "completed";
    JobStatus["failed"] = "failed";
    JobStatus["cancelled"] = "cancelled";
    JobStatus["init"] = "init";
})(JobStatus = exports.JobStatus || (exports.JobStatus = {}));

var JobWrap = function () {
    function JobWrap(jobInDb, jobMessage) {
        (0, _classCallCheck3.default)(this, JobWrap);

        this.jobInDb = {};
        this.jobInDb = jobInDb;
        this.jobMessage = jobMessage;
    }

    (0, _createClass3.default)(JobWrap, [{
        key: "getRemains",
        value: function getRemains() {
            return new Date().getTime() - this.jobInDb.nextRun.getTime();
        }
    }, {
        key: "id",
        get: function get() {
            return this.jobInDb.id;
        },
        set: function set(id) {
            this.jobInDb.id = id;
        }
    }, {
        key: "taskId",
        get: function get() {
            return this.jobInDb.taskId;
        },
        set: function set(taskId) {
            this.jobInDb.taskId = taskId;
        }
    }, {
        key: "data",
        get: function get() {
            return this.jobInDb.data;
        },
        set: function set(data) {
            this.jobInDb.data = data;
        }
    }, {
        key: "type",
        get: function get() {
            return this.jobInDb.type;
        },
        set: function set(type) {
            this.jobInDb.type = type;
        }
    }, {
        key: "schedule",
        get: function get() {
            return this.jobInDb.schedule;
        },
        set: function set(schedule) {
            this.jobInDb.schedule = schedule;
        }
    }, {
        key: "runs",
        get: function get() {
            return this.jobInDb.runs;
        },
        set: function set(runs) {
            this.jobInDb.runs = runs;
        }
    }, {
        key: "nextRun",
        get: function get() {
            return this.jobInDb.nextRun;
        },
        set: function set(nextRun) {
            this.jobInDb.nextRun = nextRun;
        }
    }, {
        key: "status",
        get: function get() {
            return this.jobInDb.status;
        },
        set: function set(status) {
            this.jobInDb.status = status;
        }
    }, {
        key: "repeats",
        get: function get() {
            return this.jobInDb.repeats;
        },
        set: function set(repeats) {
            this.jobInDb.repeats = repeats;
        }
    }, {
        key: "retries",
        get: function get() {
            return this.jobInDb.retries;
        },
        set: function set(retries) {
            this.jobInDb.retries = retries;
        }
    }]);
    return JobWrap;
}();

exports.JobWrap = JobWrap;

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("babel-runtime/regenerator");

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _toConsumableArray2 = __webpack_require__(11);

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(1);

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

Object.defineProperty(exports, "__esModule", { value: true });
var Job_1 = __webpack_require__(3);
var Bluebird = __webpack_require__(2);

var WorkerPool = function () {
    function WorkerPool(option, executor, eventEmitter) {
        (0, _classCallCheck3.default)(this, WorkerPool);

        console.log('new workerpool');
        this.workers = [];
        this.jobBuffer = [];
        this.option = option;
        var _option$concurrency = option.concurrency,
            concurrency = _option$concurrency === undefined ? 1 : _option$concurrency;

        for (var i = 0; i < concurrency; i++) {
            this.workers.push(new Worker(executor, this.jobBuffer, eventEmitter));
        }
        console.log('new workerpool end');
    }

    (0, _createClass3.default)(WorkerPool, [{
        key: "appendJobs",
        value: function appendJobs(jobs) {
            var _jobBuffer;

            console.log('appendJobs', jobs.length);
            (_jobBuffer = this.jobBuffer).push.apply(_jobBuffer, (0, _toConsumableArray3.default)(jobs));
        }
    }, {
        key: "idle",
        value: function idle() {
            return this.jobBuffer.length === 0;
        }
    }, {
        key: "prefetch",
        get: function get() {
            return this.option.prefetch || 1;
        }
    }]);
    return WorkerPool;
}();

exports.WorkerPool = WorkerPool;
var WorkerStatus;
(function (WorkerStatus) {
    WorkerStatus["IDLE"] = "idle";
    WorkerStatus["RUNNING"] = "running";
})(WorkerStatus = exports.WorkerStatus || (exports.WorkerStatus = {}));

var Worker = function () {
    function Worker(executor, jobBuffer, eventEmitter) {
        (0, _classCallCheck3.default)(this, Worker);

        this.executor = executor;
        this.status = WorkerStatus.IDLE;
        this.jobBuffer = jobBuffer;
        this.eventEmitter = eventEmitter;
        this.run();
    }

    (0, _createClass3.default)(Worker, [{
        key: "run",
        value: function run() {
            var _this = this;

            var job = this.jobBuffer.pop();
            if (!job) return Bluebird.delay(500).then(function () {
                return _this.run();
            });
            this.status = WorkerStatus.RUNNING;
            var jobRes = new Job_1.JobExecResult(job.nextRun);
            this.eventEmitter.emit(Job_1.JobEvents.RUNNING, job);
            return Bluebird.try(function () {
                return _this.executor(job, jobRes);
            }).then(function () {
                if (jobRes.status === Job_1.JobResultStatus.init) {
                    jobRes.success();
                }
                _this.eventEmitter.emit(Job_1.JobEvents.COMPLETE, job, jobRes);
            }, function (err) {
                jobRes.fail(undefined, err);
                _this.eventEmitter.emit(Job_1.JobEvents.COMPLETE, job, jobRes);
            }).finally(function () {
                _this.status = WorkerStatus.IDLE;
            }).then(function () {
                return _this.run();
            });
        }
    }]);
    return Worker;
}();

exports.Worker = Worker;

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("lodash.pick");

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function __export(m) {
    for (var p in m) {
        if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(8));
__export(__webpack_require__(3));
__export(__webpack_require__(5));
var Azure = __webpack_require__(14);
exports.Azure = Azure;
var Bluebird = __webpack_require__(2);
Bluebird.config({
    warnings: {
        wForgottenReturn: false
    }
});

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _slicedToArray2 = __webpack_require__(9);

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _regenerator = __webpack_require__(4);

var _regenerator2 = _interopRequireDefault(_regenerator);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(1);

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var __awaiter = undefined && undefined.__awaiter || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
            try {
                step(generator.next(value));
            } catch (e) {
                reject(e);
            }
        }
        function rejected(value) {
            try {
                step(generator["throw"](value));
            } catch (e) {
                reject(e);
            }
        }
        function step(result) {
            result.done ? resolve(result.value) : new P(function (resolve) {
                resolve(result.value);
            }).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var Job_1 = __webpack_require__(3);
var Worker_1 = __webpack_require__(5);
var events_1 = __webpack_require__(12);
var uuidv1 = __webpack_require__(13);
var Bluebird = __webpack_require__(2);
var pick = __webpack_require__(6);

var JobCollection = function () {
    function JobCollection(storeAdaptor, queueAdaptor, config) {
        var _this = this;

        (0, _classCallCheck3.default)(this, JobCollection);

        this.interval = 500;
        this.workerPoolMap = new Map();
        this.config = config;
        this.eventEmitter = new events_1.EventEmitter();
        this.storeAdaptor = storeAdaptor;
        this.queueAdaptor = queueAdaptor;
        this.eventEmitter.on(Job_1.JobEvents.COMPLETE, function (job, result) {
            return _this.onComplete(job, result);
        });
        this.eventEmitter.on(Job_1.JobEvents.RUNNING, function (job) {
            return _this.onJobStart(job);
        });
        try {
            this.storeAdaptor.onInit(this);
        } catch (e) {
            console.log('storeAdaptor.onInit failed');
        }
        try {
            this.queueAdaptor.onInit(this);
        } catch (e) {
            console.log('queueAdaptor.onInit failed');
        }
        this.loop();
    }

    (0, _createClass3.default)(JobCollection, [{
        key: "getQueueName",
        value: function getQueueName(type) {
            var queueName = void 0;
            if (typeof this.config.queueName === 'function') {
                queueName = this.config.queueName(type, this.config);
            } else {
                queueName = this.config.queueName;
            }
            return queueName;
        }
    }, {
        key: "fetchFullJobAndDecide",
        value: function fetchFullJobAndDecide(message) {
            return __awaiter(this, void 0, void 0, /*#__PURE__*/_regenerator2.default.mark(function _callee() {
                var jobId, job;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                jobId = this.queueAdaptor.getJobId(message);
                                _context.next = 3;
                                return this.storeAdaptor.findById(jobId);

                            case 3:
                                job = _context.sent;

                                if (!(!job || job.status !== Job_1.JobStatus.waiting)) {
                                    _context.next = 8;
                                    break;
                                }

                                console.log('clear job', message, job);
                                this.queueAdaptor.cancelTask(message);
                                return _context.abrupt("return");

                            case 8:
                                return _context.abrupt("return", new Job_1.JobWrap(job, message));

                            case 9:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));
        }
        /**
         * TODO: job pull control
         * @return {Bluebird<void>}
         */

    }, {
        key: "loop",
        value: function loop() {
            var _this2 = this;

            var pullAndDeliveryJob = function pullAndDeliveryJob(jobType, workerPool) {
                return __awaiter(_this2, void 0, void 0, /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
                    var _this3 = this;

                    var res, i, msgs, jobsToRun, jobsToDelay, jobs, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, jobWrap, remainMillSecs, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, _ref, job, delay;

                    return _regenerator2.default.wrap(function _callee2$(_context2) {
                        while (1) {
                            switch (_context2.prev = _context2.next) {
                                case 0:
                                    res = [];

                                    for (i = 0; i < workerPool.prefetch; i++) {
                                        res.push(Bluebird.resolve(this.queueAdaptor.pull(jobType, this)));
                                    }
                                    _context2.next = 4;
                                    return Bluebird.all(res.map(function (promise) {
                                        return promise.reflect();
                                    })).reduce(function (acc, inspection) {
                                        if (inspection.isFulfilled()) {
                                            acc.push(inspection.value());
                                        }
                                        return acc;
                                    }, []);

                                case 4:
                                    msgs = _context2.sent;
                                    jobsToRun = [];
                                    jobsToDelay = [];
                                    _context2.next = 9;
                                    return Bluebird.map(msgs, function (msg) {
                                        return _this3.fetchFullJobAndDecide(msg);
                                    });

                                case 9:
                                    jobs = _context2.sent;
                                    _iteratorNormalCompletion = true;
                                    _didIteratorError = false;
                                    _iteratorError = undefined;
                                    _context2.prev = 13;
                                    _iterator = jobs[Symbol.iterator]();

                                case 15:
                                    if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                                        _context2.next = 24;
                                        break;
                                    }

                                    jobWrap = _step.value;

                                    if (jobWrap) {
                                        _context2.next = 19;
                                        break;
                                    }

                                    return _context2.abrupt("continue", 21);

                                case 19:
                                    remainMillSecs = jobWrap.getRemains();

                                    if (remainMillSecs >= 0) {
                                        jobsToRun.push(jobWrap);
                                    } else {
                                        jobsToDelay.push({ job: jobWrap, delay: remainMillSecs });
                                    }

                                case 21:
                                    _iteratorNormalCompletion = true;
                                    _context2.next = 15;
                                    break;

                                case 24:
                                    _context2.next = 30;
                                    break;

                                case 26:
                                    _context2.prev = 26;
                                    _context2.t0 = _context2["catch"](13);
                                    _didIteratorError = true;
                                    _iteratorError = _context2.t0;

                                case 30:
                                    _context2.prev = 30;
                                    _context2.prev = 31;

                                    if (!_iteratorNormalCompletion && _iterator.return) {
                                        _iterator.return();
                                    }

                                case 33:
                                    _context2.prev = 33;

                                    if (!_didIteratorError) {
                                        _context2.next = 36;
                                        break;
                                    }

                                    throw _iteratorError;

                                case 36:
                                    return _context2.finish(33);

                                case 37:
                                    return _context2.finish(30);

                                case 38:
                                    _iteratorNormalCompletion2 = true;
                                    _didIteratorError2 = false;
                                    _iteratorError2 = undefined;
                                    _context2.prev = 41;

                                    for (_iterator2 = jobsToDelay[Symbol.iterator](); !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                                        _ref = _step2.value;
                                        job = _ref.job;
                                        delay = _ref.delay;

                                        try {
                                            this.queueAdaptor.delayJob(job, delay);
                                        } catch (e) {}
                                    }
                                    _context2.next = 49;
                                    break;

                                case 45:
                                    _context2.prev = 45;
                                    _context2.t1 = _context2["catch"](41);
                                    _didIteratorError2 = true;
                                    _iteratorError2 = _context2.t1;

                                case 49:
                                    _context2.prev = 49;
                                    _context2.prev = 50;

                                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                        _iterator2.return();
                                    }

                                case 52:
                                    _context2.prev = 52;

                                    if (!_didIteratorError2) {
                                        _context2.next = 55;
                                        break;
                                    }

                                    throw _iteratorError2;

                                case 55:
                                    return _context2.finish(52);

                                case 56:
                                    return _context2.finish(49);

                                case 57:
                                    workerPool.appendJobs(jobsToRun);

                                case 58:
                                case "end":
                                    return _context2.stop();
                            }
                        }
                    }, _callee2, this, [[13, 26, 30, 38], [31,, 33, 37], [41, 45, 49, 57], [50,, 52, 56]]);
                }));
            };
            var action = function action() {
                var res = [];
                var _iteratorNormalCompletion3 = true;
                var _didIteratorError3 = false;
                var _iteratorError3 = undefined;

                try {
                    for (var _iterator3 = _this2.workerPoolMap[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                        var _ref2 = _step3.value;

                        var _ref3 = (0, _slicedToArray3.default)(_ref2, 2);

                        var jobType = _ref3[0];
                        var workerPool = _ref3[1];

                        try {
                            res.push(pullAndDeliveryJob(jobType, workerPool));
                        } catch (e) {}
                        ;
                    }
                } catch (err) {
                    _didIteratorError3 = true;
                    _iteratorError3 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion3 && _iterator3.return) {
                            _iterator3.return();
                        }
                    } finally {
                        if (_didIteratorError3) {
                            throw _iteratorError3;
                        }
                    }
                }

                return Bluebird.all(res).delay(_this2.interval).then(function () {
                    return action();
                });
            };
            return Bluebird.try(action);
        }
    }, {
        key: "onComplete",
        value: function onComplete(job, result) {
            //TODO: exception handler
            this.storeAdaptor.onComplete(job, result);
            this.queueAdaptor.onComplete(job, result);
            //resubmit repeated job
            try {
                if (job.schedule.repeatOpt.repeats > job.repeats && job.schedule.repeatOpt.until > new Date()) {
                    //remain the same task id
                    var _newJob = pick(job, ['taskId', 'data', 'type', 'schedule', 'status']);
                    _newJob.runs = [];
                    _newJob.repeats = job.repeats + 1;
                    _newJob.retries = 0;
                    _newJob.id = uuidv1();
                    var newJob = new Job_1.JobWrap(_newJob);
                    this.calcNextRun(newJob, result.completeAt);
                    if (newJob.nextRun) {
                        this.reSubmit(newJob);
                    }
                }
            } catch (e) {}
        }
    }, {
        key: "onJobStart",
        value: function onJobStart(job) {
            this.storeAdaptor.beforeStart(job);
            this.queueAdaptor.beforeStart(job);
        }
    }, {
        key: "submit",
        value: function submit(type, data, scheduleOption) {
            return __awaiter(this, void 0, void 0, /*#__PURE__*/_regenerator2.default.mark(function _callee3() {
                var _this4 = this;

                var job;
                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                //TODO: check type not empty
                                //TODO: calculate next run date
                                //TODO: build jobInDb in storeAdaptor
                                job = new Job_1.JobWrap({ id: uuidv1(), taskId: uuidv1(), type: type, data: data, status: Job_1.JobStatus.init, runs: [], schedule: scheduleOption, retries: 0, repeats: 0 }, undefined);

                                this.calcNextRun(job);
                                _context3.next = 4;
                                return this.storeAdaptor.beforeSubmit(this, job);

                            case 4:
                                _context3.next = 6;
                                return this.queueAdaptor.beforeSubmit(this, job);

                            case 6:
                                job.status = Job_1.JobStatus.waiting;
                                _context3.next = 9;
                                return Bluebird.try(function () {
                                    return _this4.storeAdaptor.save(job.jobInDb);
                                });

                            case 9:
                                _context3.next = 11;
                                return Bluebird.try(function () {
                                    return _this4.queueAdaptor.sendTask(_this4, job);
                                });

                            case 11:
                                return _context3.abrupt("return", job);

                            case 12:
                            case "end":
                                return _context3.stop();
                        }
                    }
                }, _callee3, this);
            }));
        }
    }, {
        key: "reSubmit",
        value: function reSubmit(job) {
            return __awaiter(this, void 0, void 0, /*#__PURE__*/_regenerator2.default.mark(function _callee4() {
                var _this5 = this;

                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _context4.next = 2;
                                return this.storeAdaptor.beforeSubmit(this, job);

                            case 2:
                                _context4.next = 4;
                                return this.queueAdaptor.beforeSubmit(this, job);

                            case 4:
                                job.status = Job_1.JobStatus.waiting;
                                _context4.next = 7;
                                return Bluebird.try(function () {
                                    return _this5.storeAdaptor.save(job.jobInDb);
                                });

                            case 7:
                                _context4.next = 9;
                                return Bluebird.try(function () {
                                    return _this5.queueAdaptor.sendTask(_this5, job);
                                });

                            case 9:
                                return _context4.abrupt("return", job);

                            case 10:
                            case "end":
                                return _context4.stop();
                        }
                    }
                }, _callee4, this);
            }));
        }
    }, {
        key: "calcNextRun",
        value: function calcNextRun(job, lastRun) {
            var scheduleOpt = job.schedule;
            //first run time calc
            if (job.status === Job_1.JobStatus.init) {
                if (scheduleOpt.after) {
                    job.nextRun = scheduleOpt.after;
                } else {
                    job.nextRun = new Date();
                }
            } else if (job.status === Job_1.JobStatus.completed && lastRun) {
                job.nextRun = new Date(job.schedule.repeatOpt.wait + lastRun.getTime());
            } else {
                job.nextRun = null;
            }
        }
    }, {
        key: "cancel",
        value: function cancel(jobId) {
            throw new Error();
        }
    }, {
        key: "find",
        value: function find(jobId) {
            return __awaiter(this, void 0, void 0, /*#__PURE__*/_regenerator2.default.mark(function _callee5() {
                return _regenerator2.default.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                return _context5.abrupt("return", this.storeAdaptor.findById(jobId));

                            case 1:
                            case "end":
                                return _context5.stop();
                        }
                    }
                }, _callee5, this);
            }));
        }
    }, {
        key: "listen",
        value: function listen(jobType, option, executor) {
            console.log('listen');
            if (!!this.workerPoolMap.get(jobType)) {
                throw new Error('duplicated register executor');
            }
            var workerPool = new Worker_1.WorkerPool(option, executor, this.eventEmitter);
            console.log('set w p map', jobType, workerPool);
            this.workerPoolMap.set(jobType, workerPool);
        }
    }]);
    return JobCollection;
}();

exports.JobCollection = JobCollection;

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/slicedToArray");

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", { value: true });
// This is the JS max int value = 2^53
exports.FOREVER = 9007199254740992;
//This is the maximum date value in JS
exports.FOREVER_DATE = new Date(8640000000000000);

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/toConsumableArray");

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = require("events");

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = require("uuid/v1");

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _regenerator = __webpack_require__(4);

var _regenerator2 = _interopRequireDefault(_regenerator);

var _classCallCheck2 = __webpack_require__(0);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(1);

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var __awaiter = undefined && undefined.__awaiter || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
            try {
                step(generator.next(value));
            } catch (e) {
                reject(e);
            }
        }
        function rejected(value) {
            try {
                step(generator["throw"](value));
            } catch (e) {
                reject(e);
            }
        }
        function step(result) {
            result.done ? resolve(result.value) : new P(function (resolve) {
                resolve(result.value);
            }).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var azure_sb_1 = __webpack_require__(15);
var Bluebird = __webpack_require__(2);
var pick = __webpack_require__(6);

var AzureQueueAdaptor = function () {
    function AzureQueueAdaptor() {
        (0, _classCallCheck3.default)(this, AzureQueueAdaptor);

        this.queueService = Bluebird.promisifyAll(azure_sb_1.createServiceBusService());
    }

    (0, _createClass3.default)(AzureQueueAdaptor, [{
        key: "onInit",
        value: function onInit(jc) {
            this.queueInitLog = new Map();
        }
        //default LockDuration is 1 min

    }, {
        key: "beforeSubmit",
        value: function beforeSubmit(jc, job) {
            return __awaiter(this, void 0, void 0, /*#__PURE__*/_regenerator2.default.mark(function _callee() {
                var queueName;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                queueName = jc.getQueueName(job.type);

                                if (this.queueInitLog.get(queueName)) {
                                    _context.next = 5;
                                    break;
                                }

                                _context.next = 4;
                                return this.queueService.createQueueIfNotExistsAsync(queueName);

                            case 4:
                                this.queueInitLog.set(queueName, true);

                            case 5:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));
        }
    }, {
        key: "sendTask",
        value: function sendTask(jc, job) {
            return __awaiter(this, void 0, void 0, /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
                var queueName, schedule, msgBody, message;
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                queueName = jc.getQueueName(job.type);
                                schedule = void 0;
                                msgBody = pick(job, ['nextRun', 'schedule', 'id']);

                                if (job.nextRun > new Date()) {
                                    schedule = job.nextRun.toUTCString();
                                } else {
                                    schedule = undefined;
                                }
                                message = { body: '', customProperties: msgBody, brokerProperties: { ScheduledEnqueueTimeUtc: schedule } };
                                _context2.next = 7;
                                return this.queueService.sendQueueMessageAsync(queueName, message);

                            case 7:
                            case "end":
                                return _context2.stop();
                        }
                    }
                }, _callee2, this);
            }));
        }
    }, {
        key: "onComplete",
        value: function onComplete(job, result) {
            return __awaiter(this, void 0, void 0, /*#__PURE__*/_regenerator2.default.mark(function _callee3() {
                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.next = 2;
                                return this.queueService.deleteMessageAsync(job.jobMessage);

                            case 2:
                                return _context3.abrupt("return", null);

                            case 3:
                            case "end":
                                return _context3.stop();
                        }
                    }
                }, _callee3, this);
            }));
        }
    }, {
        key: "beforeStart",
        value: function beforeStart(job) {}
    }, {
        key: "pull",
        value: function pull(jobType, jc) {
            return __awaiter(this, void 0, void 0, /*#__PURE__*/_regenerator2.default.mark(function _callee4() {
                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _context4.next = 2;
                                return this.queueService.receiveQueueMessageAsync(jc.getQueueName(jobType), { isPeekLock: true });

                            case 2:
                                return _context4.abrupt("return", _context4.sent);

                            case 3:
                            case "end":
                                return _context4.stop();
                        }
                    }
                }, _callee4, this);
            }));
        }
    }, {
        key: "getJobId",
        value: function getJobId(message) {
            return message.customProperties.id;
        }
    }, {
        key: "cancelTask",
        value: function cancelTask(message) {
            return __awaiter(this, void 0, void 0, /*#__PURE__*/_regenerator2.default.mark(function _callee5() {
                return _regenerator2.default.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                _context5.next = 2;
                                return this.queueService.deleteMessageAsync(message);

                            case 2:
                                return _context5.abrupt("return", _context5.sent);

                            case 3:
                            case "end":
                                return _context5.stop();
                        }
                    }
                }, _callee5, this);
            }));
        }
    }, {
        key: "delayJob",
        value: function delayJob(job, delayMills) {
            throw new Error('operation not supported');
        }
    }]);
    return AzureQueueAdaptor;
}();

exports.AzureQueueAdaptor = AzureQueueAdaptor;

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = require("azure-sb");

/***/ })
/******/ ]);