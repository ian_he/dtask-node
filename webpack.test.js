const webpack = require("webpack");
const path = require("path");
const nodeEnv = process.env.NODE_ENV || "development";
const isProd = nodeEnv === "production";

var config = {
    devtool: isProd ? "hidden-source-map" : "source-map",
    entry: {
        index: "./index.ts"
    },
    output: {
        path: path.resolve("./dist"),
        filename: "[name].js",
        sourceMapFilename: "[name].map",
        libraryTarget: 'commonjs2'
    },
    module: {
        rules: [
            {
                enforce: "pre",
                test: /\.ts?$/,
                exclude: ["node_modules"],
                use: ["awesome-typescript-loader", "source-map-loader"]
            },
        ]
    },
    resolve: {
        extensions: [".ts", ".js"]
    },
    target: 'node'
};

module.exports = config;
