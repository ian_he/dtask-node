// This is the JS max int value = 2^53
export const FOREVER = 9007199254740992;

//This is the maximum date value in JS
export const FOREVER_DATE = new Date(8640000000000000);