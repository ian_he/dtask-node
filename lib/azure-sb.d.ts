declare module 'azure-sb' {
    export function createServiceBusService(configOrNamespaceOrConnectionString?:string): ServiceBusService;

    export interface ServiceBusService {
        createQueueIfNotExists: (queue: string, option: any, callback: (error: any, response: any) => void) => void;
        getQueue:(queuePath:string, callback:(error:any, getqueueresult:any, response:any)=>void)=>void
        sendQueueMessage: (queuePath: string, message: QueueMessage, callback: (error: any, response: any) => void) => void;
        receiveQueueMessage: (queuePath: string, optionsOrCallback: ReceiveQueueMessageOption, callback?: QueueMessageCallback) => void;
        deleteMessage:(message:string|QueueMessage, callback:(error:any, response:any)=>void)=>void;
    }

    export interface QueueMessage {
        body: string;
        customProperties?: object;
        brokerProperties?: {
            CorrelationId?: string
            SessionId?: string
            MessageId?: string
            Label?: string
            ReplyTo?: string
            TimeToLive?: string
            To?: string
            ScheduledEnqueueTimeUtc?: string
            ReplyToSessionId?: string
        }
    }

    export interface ReceiveQueueMessageOption {
        isPeekLock?: boolean;
        timeoutIntervalInS?: number;
    }

    export type QueueMessageCallback = (error: any,
                                        result: ReceiveQueueMessage,
                                        response: any) => void

    export interface ReceiveQueueMessage extends QueueMessage{
        location?: string
    }
}