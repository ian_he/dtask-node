import Bluebird = require('bluebird');

export function wrapLoop(func: (...args: any[]) => Bluebird<any>, args: any[], verifyFunc: (res: any) => boolean,
                         interval = 1000, timeout = 30 * 1000) {
    const start = new Date().getTime();
    const wrap:()=>Bluebird<any> = () => func(...args).then(result => {
        if (verifyFunc(result)) {
            return result;
        } else {
            if ((new Date().getTime() - start) < timeout) {
                return Bluebird.delay(interval).then(wrap);
            } else {
                throw new Error('timeout');
            }
        }
    });
    return wrap();
}

export function loopForever(func: (...args: any[]) => Bluebird<any>, args: any[],
                         interval = 1000) {
    const wrap:()=>Bluebird<any> = () => func(...args).then(result => {
                return Bluebird.delay(interval).then(wrap);
    });
    return wrap();
}