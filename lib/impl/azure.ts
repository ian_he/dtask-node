import {createServiceBusService, QueueMessage, ReceiveQueueMessage, ReceiveQueueMessageOption} from "azure-sb";
import {JobCollectionIntf, JobQueueIntf} from "../JobCollection";
import {JobExecResult, JobIntf, JobWrap} from "../Job";
import Bluebird = require("bluebird");
import pick = require('lodash.pick');

export interface ServiceBusServiceAsync {
    createQueueIfNotExistsAsync: (queue: string, option?: any) => Bluebird<any>;
    sendQueueMessageAsync: (queuePath: string, message: QueueMessage) => Bluebird<any>;
    receiveQueueMessageAsync: (queuePath: string, options?: ReceiveQueueMessageOption) => Bluebird<ReceiveQueueMessage>;
    deleteMessageAsync: (message: string | QueueMessage) => Bluebird<any>;
}

export class AzureQueueAdaptor implements JobQueueIntf<ReceiveQueueMessage> {
    queueService: ServiceBusServiceAsync;
    queueInitLog: Map<string, boolean>;

    constructor(){
        this.queueService = Bluebird.promisifyAll(createServiceBusService()) as ServiceBusServiceAsync;
    }

    onInit(jc: JobCollectionIntf) {
        this.queueInitLog = new Map();
    }

    //default LockDuration is 1 min
    async beforeSubmit(jc: JobCollectionIntf, job: JobIntf) {
        const queueName = jc.getQueueName(job.type);
        if (!this.queueInitLog.get(queueName)) {
            await this.queueService.createQueueIfNotExistsAsync(queueName);
            this.queueInitLog.set(queueName, true);
        }
    }

    async sendTask(jc: JobCollectionIntf, job: JobWrap<ReceiveQueueMessage>) {
        const queueName = jc.getQueueName(job.type);
        let schedule;
        const msgBody = pick(job, ['nextRun', 'schedule', 'id']);
        if(job.nextRun > new Date()){
            schedule = job.nextRun.toUTCString();
        }else{
            schedule = undefined;
        }
        const message: QueueMessage = {body: '', customProperties: msgBody, brokerProperties: {ScheduledEnqueueTimeUtc: schedule}};
        await this.queueService.sendQueueMessageAsync(queueName, message);
    }

    async onComplete(job: JobWrap<ReceiveQueueMessage>, result: JobExecResult):Promise<void> {
        // if (result.status === JobResultStatus.success) {
        //     await this.queueService.deleteMessageAsync(job.jobMessage);
        // } else {
        //     //TODO: support retry
        //     this.queueService.deleteMessageAsync(job.jobMessage);
        // }
        await this.queueService.deleteMessageAsync(job.jobMessage);
        return null;
    }

    beforeStart(job: JobWrap<any>){
    }

    async pull(jobType: string, jc: JobCollectionIntf) {
        return await this.queueService.receiveQueueMessageAsync(jc.getQueueName(jobType),
            {isPeekLock: true})
    }

    getJobId(message: ReceiveQueueMessage){
        return (message.customProperties as {id:string}).id;
    }

    async cancelTask(message: ReceiveQueueMessage){
        return await this.queueService.deleteMessageAsync(message);
    }

    delayJob(job: JobWrap<ReceiveQueueMessage>, delayMills: number){
        throw new Error('operation not supported');
    }
}