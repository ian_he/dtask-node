// import {JobCollectionBase, JobCollectionConfigIntf, JobCollectionIntf} from "../JobCollection";
// import {JobExecResult, JobId, JobIntf, JobResultStatus, JobStatus, ScheduleConfig, JobWrap} from "../Job";
// import {Executor, WorkerPool} from "../Worker";
// import {createServiceBusService, QueueMessage, ReceiveQueueMessage, ReceiveQueueMessageOption} from 'azure-sb';
// import Bluebird = require("bluebird");
// import pick = require('lodash.pick');
// import uuidv1 = require('uuid/v1');
//
//
// class JobInQueue {
//     id: string;
//     schedule: ScheduleConfig;
//     nextRun: Date;
// }
//
// export class JobCollection extends JobCollectionBase implements JobCollectionIntf {
//
//     interval = 500;
//
//
//     constructor(config: JobCollectionConfig, jobStore: JobStoreIntf) {
//         super();
//         this.config = config;
//         this.queuePrefix = config.queuePrefix;
//         this.jobStore = jobStore;
//         this.queueService = Bluebird.promisifyAll(createServiceBusService()) as ServiceBusServiceAsync;
//         this.queueInitLog = new Map();
//         this.loop();
//         this.eventEmitter.on('job_done',(job, res)=>this.complete(job, res));
//     }
//
//     fetchFullJobAndDecide(message: ReceiveQueueMessage): JobIntf {
//         const jobInQueue = message.customProperties as JobInQueue;
//         const job = this.jobStore.findById(jobInQueue.id);
//
//         if (!job || job.status !== JobStatus.waiting) {
//             console.log('clear job', jobInQueue, job);
//             this.queueService.deleteMessageAsync(message);
//             return;
//         }
//         if (job.nextRun > new Date()) {
//             return;
//         }
//         return new JobWrap(job, message);
//
//     }
//
//     /**
//      * TODO: job pull control
//      * @return {Bluebird<void>}
//      */
//     loop(): Bluebird<void> {
//         const pullAndDeliveryJob = async (jobType: string, workerPool: WorkerPool) => {
//             const res: Bluebird<ReceiveQueueMessage>[] = [];
//             for (let i = 0; i < workerPool.prefetch; i++) {
//                 res.push(this.queueService.receiveQueueMessageAsync(this.getQueueName(jobType),
//                     {isPeekLock: true}));
//             }
//             const msgs: ReceiveQueueMessage[] = await Bluebird.all(res.map(function(promise) {
//                 return promise.reflect();
//             })).reduce(function(acc:ReceiveQueueMessage[], inspection:Bluebird.Inspection<ReceiveQueueMessage>) {
//                 if (inspection.isFulfilled()) {
//                     acc.push(inspection.value());
//                 }
//                 return acc;
//             },[]);
//             const jobs = [];
//             for (let msg of msgs) {
//                 jobs.push(this.fetchFullJobAndDecide(msg));
//
//             }
//             workerPool.appendJobs(jobs);
//         };
//         const action = (): Bluebird<any> => {
//             console.log('loop action', this.workerPoolMap);
//             const res = [];
//             for (let [jobType, workerPool] of this.workerPoolMap) {
//                 res.push(pullAndDeliveryJob(jobType, workerPool));
//             }
//             return Bluebird.all(res).delay(this.interval).then(()=>action());
//
//         };
//         return Bluebird.try(action);
//     }
//
//     newJob(type: string, data: object): JobIntf {
//         const job = super.newJob(type, data);
//         return job;
//     }
//
//     getQueueName(jobType: string) {
//         return `${this.queuePrefix}_${jobType}`;
//     }
//
//     async submit(job: JobIntf): Promise<void> {
//         //TODO: check type not empty
//         //TODO: calculate next run date
//         job.status = JobStatus.waiting;
//         job.id = uuidv1();
//         await Bluebird.try(()=>this.jobStore.save(job));
//         const msgBody: JobInQueue = pick(job, ['nextRun', 'schedule', 'id']);
//         const message: QueueMessage = {body: '', customProperties: msgBody};
//         const queueName = this.getQueueName(job.type);
//         if (!this.queueInitLog.get(queueName)) {
//             await this.queueService.createQueueIfNotExistsAsync(queueName);
//             this.queueInitLog.set(queueName, true);
//         }
//         await this.queueService.sendQueueMessageAsync(queueName, message);
//     }
//
//     find(jobId: string | number): JobIntf {
//         return this.jobStore.findById(jobId);
//     }
//
//     cancel(jobId: string | number): void {
//         const job = this.jobStore.findById(jobId);
//         job.status = JobStatus.cancelled;
//         this.jobStore.save(job);
//     }
//
//     // complete(job: JobIntf, result: JobExecResult): Promise<void>|void {
//     //     console.log('on complete', job, result);
//     //     if (!result || !(job instanceof JobWrap)) throw new Error();
//     //     const jobWrap = job as JobWrap;
//     //     console.log('before call before');
//     //     if (result.status === JobResultStatus.success) {
//     //         this.queueService.deleteMessageAsync(jobWrap.jobMessage);
//     //         console.log('before call SAVE 1.1',this.jobStore);
//     //         jobWrap.status = JobStatus.completed;
//     //         jobWrap.lastRun = new Date();
//     //         jobWrap.runs.push(result);
//     //         console.log('before call save 1.2',this.jobStore);
//     //         this.jobStore.save(jobWrap.jobInDb);
//     //     } else {
//     //         this.queueService.deleteMessageAsync(jobWrap.jobMessage);
//     //         jobWrap.status = JobStatus.failed;
//     //         jobWrap.lastRun = new Date();
//     //         jobWrap.runs.push(result);
//     //         console.log('before call save2',this.jobStore);
//     //         this.jobStore.save(jobWrap.jobInDb);
//     //     }
//     // }
//
// }
//
// // export async function testttt() {
// //
// //     const queueService = createServiceBusService();
// //     const queueService2 = Bluebird.promisifyAll(queueService) as ServiceBusServiceAsync;
// //     var message = {
// //         body: 'Test message11',
// //         customProperties: {
// //             testproperty: 'TestValue'
// //         }
// //     };
// //     const res = await  queueService2.sendQueueMessageAsync('bgtask', message);
// //     const lockedMessage = await queueService2.receiveQueueMessageAsync('bgtask', {isPeekLock: true});
// //     console.log('msg:', JSON.stringify(lockedMessage));
// //     // Message received and locked
// //     return await queueService2.deleteMessageAsync(lockedMessage);
// // }
