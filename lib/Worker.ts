import {JobEvents, JobExecResult, JobIntf, JobResultStatus, JobStatus} from "./Job";
import {EventEmitter} from 'events';
import Bluebird = require("bluebird");

export type Executor = (job: JobIntf, res: JobExecResult) => void | Bluebird<void>;

export class WorkerPool {
    workers: Worker[];
    jobBuffer: JobIntf[];
    option: WorkerPoolOption;

    constructor(option: WorkerPoolOption, executor: Executor, eventEmitter: EventEmitter) {
        console.log('new workerpool');
        this.workers = [];
        this.jobBuffer = [];
        this.option = option;
        const {concurrency = 1} = option;
        for (let i = 0; i < concurrency; i++) {
            this.workers.push(new Worker(executor, this.jobBuffer, eventEmitter));
        }
        console.log('new workerpool end');
    }

    appendJobs(jobs: JobIntf[]): void {
        console.log('appendJobs', jobs.length);
        this.jobBuffer.push(...jobs);
    }

    idle(): boolean {
        return this.jobBuffer.length === 0;
    }

    get prefetch(): number {
        return this.option.prefetch || 1;
    }

    // new (jc: JobCollectionIntf, type: string, executor: Executor)

    // start(): void
    // stop(): void
}

export interface WorkerPoolOption {
    concurrency: number
    prefetch: number
}

export enum WorkerStatus {
    IDLE = 'idle',
    RUNNING = 'running'
}

export class Worker {
    executor: Executor;
    status: WorkerStatus;
    jobBuffer: JobIntf[];
    eventEmitter: EventEmitter;

    constructor(executor: Executor, jobBuffer: JobIntf[], eventEmitter: EventEmitter) {
        this.executor = executor;
        this.status = WorkerStatus.IDLE;
        this.jobBuffer = jobBuffer;
        this.eventEmitter = eventEmitter;
        this.run();
    }

    run(): Bluebird<any> {
        const job = this.jobBuffer.pop();
        if (!job) return Bluebird.delay(500).then(()=>this.run());

        this.status = WorkerStatus.RUNNING;
        const jobRes = new JobExecResult(job.nextRun);
        this.eventEmitter.emit(JobEvents.RUNNING, job);
        return Bluebird.try(() => this.executor(job, jobRes))
            .then(()=>{
                if(jobRes.status === JobResultStatus.init){
                    jobRes.success();
                }
                this.eventEmitter.emit(JobEvents.COMPLETE, job, jobRes);
            }, err=>{
                jobRes.fail(undefined, err);
                this.eventEmitter.emit(JobEvents.COMPLETE, job, jobRes)
            })
            .finally(() => {
                this.status = WorkerStatus.IDLE;
            }).then(()=>this.run());
    }
}