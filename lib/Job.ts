import {FOREVER_DATE} from "./constants";

export type JobId = string | number;


export enum JobResultStatus {
    init='init',
    success='success',
    fail='fail'
}
export class JobExecResult {
    status: JobResultStatus;
    data: any;
    error: Error;
    runAt: Date;
    completeAt: Date;
    scheduleAt: Date;
    constructor(scheduleAt: Date){
        this.status = JobResultStatus.init;
        this.runAt = new Date();
        this.scheduleAt = scheduleAt;
    }

    success(data?: any){
        this.status = JobResultStatus.success;
        this.data = data;
        this.completeAt = new Date();
    }

    fail(data?: any, err?: Error){
        this.status = JobResultStatus.fail;
        this.data = data;
        this.error = err;
        this.completeAt = new Date();
    }
}

export interface JobIntf {
    //runId
    id?: JobId;
    taskId: JobId;
    data: object;
    type: string;
    schedule: ScheduleConfig;
    runs?: JobExecResult[];
    nextRun?: Date;
    status: JobStatus;
    repeats: number;
    retries: number;
}

const DEFAULT_REPEAT={
    repeats: 0, until: FOREVER_DATE, wait: 300000
};
const DEFAULT_RETRY={
    retries: 0, until: FOREVER_DATE, wait: 300000
};

export class ScheduleConfig {
    repeatOpt?: RepeatConfig;
    retryOpt ?: RetryConfig;
    after?: Date;
    constructor(){
        this.repeatOpt = {...DEFAULT_REPEAT};
        this.retryOpt = {...DEFAULT_RETRY};
    }

    setAfter(date:Date){
        this.after = date;
        return this;
    }

    retry({retries,until,wait}:RetryConfig){
        if(retries !== undefined && retries !== null){
            this.retryOpt.retries = retries;
        }
        if(until){
            this.retryOpt.until = until;
        }
        if(wait !== undefined && wait !== null){
            this.retryOpt.wait = wait;
        }
        return this;
    }

    repeat({repeats,until,wait}:RepeatConfig){
        if(repeats !== undefined && repeats !== null){
            this.repeatOpt.repeats = repeats;
        }
        if(until){
            this.repeatOpt.until = until;
        }
        if(wait !== undefined && wait !== null){
            this.repeatOpt.wait = wait;
        }
        return this;
    }
}

export interface RepeatConfig{
    repeats: number
    until: Date
    wait: number
}

export interface RetryConfig{
    retries: number
    until: Date
    wait: number
}

export enum JobEvents {
    COMPLETE = 'job_done',
    RUNNING = 'job_running'
}

export enum JobStatus {
    waiting='waiting',
    running='running',
    completed='completed',
    failed='failed',
    cancelled='cancelled',
    init='init'
}

export class JobWrap<D> implements JobIntf{

    constructor(jobInDb: JobIntf, jobMessage?: D) {
        this.jobInDb = jobInDb;
        this.jobMessage = jobMessage;
    }
    jobInDb: any = {};
    jobMessage: D;

    getRemains(): number{
        return (new Date().getTime() - this.jobInDb.nextRun.getTime());
    }

    get id(): JobId {
        return this.jobInDb.id;
    }

    set id(id: JobId) {
        this.jobInDb.id = id;
    }

    get taskId(): JobId {
        return this.jobInDb.taskId;
    }

    set taskId(taskId: JobId) {
        this.jobInDb.taskId = taskId;
    }

    get data(): object {
        return this.jobInDb.data;
    }

    set data(data: object) {
        this.jobInDb.data = data;
    }

    get type(): string {
        return this.jobInDb.type;
    }

    set type(type: string) {
        this.jobInDb.type = type;
    }

    get schedule(): ScheduleConfig {
        return this.jobInDb.schedule;
    }

    set schedule(schedule: ScheduleConfig) {
        this.jobInDb.schedule = schedule;
    }

    get runs(): JobExecResult[] {
        return this.jobInDb.runs;
    }

    set runs(runs: JobExecResult[]) {
        this.jobInDb.runs = runs;
    }

    get nextRun(): Date {
        return this.jobInDb.nextRun;
    }

    set nextRun(nextRun: Date) {
        this.jobInDb.nextRun = nextRun;
    }

    get status(): JobStatus {
        return this.jobInDb.status;
    }

    set status(status: JobStatus) {
        this.jobInDb.status = status;
    }

    get repeats(): number {
        return this.jobInDb.repeats;
    }

    set repeats(repeats: number) {
        this.jobInDb.repeats = repeats;
    }

    get retries(): number {
        return this.jobInDb.retries;
    }

    set retries(retries: number) {
        this.jobInDb.retries = retries;
    }

}