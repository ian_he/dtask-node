import {JobEvents, JobExecResult, JobId, JobIntf, JobStatus, JobWrap, ScheduleConfig} from "./Job";
import {Executor, WorkerPool, WorkerPoolOption} from "./Worker";
import {EventEmitter} from 'events';
import uuidv1 = require('uuid/v1');
import Bluebird = require("bluebird");
import pick = require('lodash.pick');

export interface JobCollectionIntf {
    // new (config:JobCollectionConfig): void
    // newJob(type: string, data: object): JobIntf

    config: JobCollectionConfigIntf;

    getQueueName(type:string):string;

    submit(type: string, data: Object, scheduleOption: ScheduleConfig): Promise<JobIntf>;

    reSubmit(job: JobWrap<any>): Promise<JobIntf>;

    cancel(jobId: JobId): void

    listen(jobType: string, option: WorkerPoolOption, executor:Executor): void

    find(jobId: JobId): Promise<JobIntf>

}

export interface JobCollectionConfigIntf {
    queueName: string| ((jobType:string,jc:JobCollectionConfigIntf)=>string);
    queuePrefix: string;
    storeName: string| ((_:JobCollectionConfigIntf)=>string);
}

export interface JobStoreIntf<T extends JobIntf> {
    onInit: (jc: JobCollectionIntf)=>void|Promise<void>;
    beforeSubmit: (jc: JobCollectionIntf, job: JobIntf)=>void|Promise<void>;
    onComplete: (job: JobWrap<any>, result: JobExecResult)=>void|Promise<void>;
    beforeStart: (job: JobWrap<any>)=>void|Promise<void>;
    findById: (id: JobId) => Promise<T>
    save: (job: T) => void
}

export interface JobQueueIntf<D> {
    onInit: (jc: JobCollectionIntf)=>void|Promise<void>;
    beforeSubmit: (jc: JobCollectionIntf, job: JobIntf)=>void|Promise<void>;
    onComplete: (job: JobWrap<D>, result: JobExecResult)=>void|Promise<void>;
    beforeStart: (job: JobWrap<D>)=>void|Promise<void>;
    sendTask: (jc: JobCollectionIntf, job: JobWrap<D>)=>void|Promise<void>;
    pull: (jobType: string, jc: JobCollectionIntf)=>Promise<D>;
    getJobId: (msg: D)=>JobId;
    cancelTask: (msg: D)=>void|Promise<void>;
    delayJob: (job: JobWrap<D>, delayMills: number)=>void|Promise<void>;
}

export class JobCollection<D> implements JobCollectionIntf {

    interval = 500;
    /**
     * key: job_type
     * value: workerpool
     */
    workerPoolMap: Map<string, WorkerPool>;
    eventEmitter: EventEmitter;
    storeAdaptor: JobStoreIntf<JobIntf>;
    queueAdaptor: JobQueueIntf<D>;
    config: JobCollectionConfigIntf;

    constructor(storeAdaptor: JobStoreIntf<JobIntf>, queueAdaptor: JobQueueIntf<D>, config: JobCollectionConfigIntf)
    {
        this.workerPoolMap = new Map();
        this.config = config;
        this.eventEmitter = new EventEmitter();
        this.storeAdaptor = storeAdaptor;
        this.queueAdaptor = queueAdaptor;
        this.eventEmitter.on(JobEvents.COMPLETE,(job: JobWrap<D>, result: JobExecResult)=>this.onComplete(job, result));
        this.eventEmitter.on(JobEvents.RUNNING,(job: JobWrap<D>)=>this.onJobStart(job));
        try{
            this.storeAdaptor.onInit(this);
        }catch (e){console.log('storeAdaptor.onInit failed')}
        try{
            this.queueAdaptor.onInit(this);
        }catch (e){console.log('queueAdaptor.onInit failed')}
        this.loop();
    }

    getQueueName(type: string): string {
        let queueName:string;
        if(typeof this.config.queueName === 'function'){
            queueName = this.config.queueName(type, this.config);
        }else{
            queueName = this.config.queueName;
        }
        return queueName;
    }

    async fetchFullJobAndDecide(message: D): Promise<JobWrap<D>> {
        const jobId = this.queueAdaptor.getJobId(message);
        const job = await this.storeAdaptor.findById(jobId);

        if (!job || job.status !== JobStatus.waiting) {
            console.log('clear job', message, job);
            this.queueAdaptor.cancelTask(message);
            return;
        }
        return new JobWrap(job, message);

    }

    /**
     * TODO: job pull control
     * @return {Bluebird<void>}
     */
    loop(): Bluebird<void> {
        const pullAndDeliveryJob = async (jobType: string, workerPool: WorkerPool) => {
            const res: Bluebird<D>[] = [];
            for (let i = 0; i < workerPool.prefetch; i++) {
                res.push(Bluebird.resolve(this.queueAdaptor.pull(jobType,this)));
            }
            const msgs: D[] = await Bluebird.all(res.map(function(promise) {
                return promise.reflect();
            })).reduce(function(acc:D[], inspection:Bluebird.Inspection<D>) {
                if (inspection.isFulfilled()) {
                    acc.push(inspection.value());
                }
                return acc;
            },[]);
            const jobsToRun = [];
            const jobsToDelay:{job: JobWrap<D>, delay: number}[] = [];
            const jobs = await Bluebird.map(msgs, msg=>this.fetchFullJobAndDecide(msg));
            for(let jobWrap of jobs){
                if(!jobWrap) continue;
                const remainMillSecs = jobWrap.getRemains();
                if(remainMillSecs >= 0){
                    jobsToRun.push(jobWrap);
                }else{
                    jobsToDelay.push({job: jobWrap, delay: remainMillSecs});
                }
            }
            for(let {job, delay} of jobsToDelay){
                try {
                    this.queueAdaptor.delayJob(job, delay);
                }catch (e){}
            }

            workerPool.appendJobs(jobsToRun);
        };
        const action = (): Bluebird<any> => {
            const res = [];
            for (let [jobType, workerPool] of this.workerPoolMap) {
                try {
                    res.push(pullAndDeliveryJob(jobType, workerPool));
                }catch (e){};
            }
            return Bluebird.all(res).delay(this.interval).then(()=>action());

        };
        return Bluebird.try(action);
    }

    onComplete(job: JobWrap<D>, result: JobExecResult){
        //TODO: exception handler
        this.storeAdaptor.onComplete(job, result);
        this.queueAdaptor.onComplete(job, result);
        //resubmit repeated job
        try {
            if (job.schedule.repeatOpt.repeats > job.repeats && job.schedule.repeatOpt.until > new Date()){
                //remain the same task id
                const _newJob = pick(job,['taskId','data','type','schedule','status']);
                _newJob.runs=[];
                _newJob.repeats = job.repeats+1;
                _newJob.retries = 0;
                _newJob.id = uuidv1();
                const newJob = new JobWrap<D>(_newJob as JobIntf);
                this.calcNextRun(newJob, result.completeAt);
                if(newJob.nextRun){
                    this.reSubmit(newJob);
                }
            }
        }catch (e){}
    }

    onJobStart(job: JobWrap<D>){
        this.storeAdaptor.beforeStart(job);
        this.queueAdaptor.beforeStart(job);
    }

    async submit(type: string, data: Object, scheduleOption: ScheduleConfig): Promise<JobWrap<D>> {
        //TODO: check type not empty
        //TODO: calculate next run date
        //TODO: build jobInDb in storeAdaptor
        const job = new JobWrap({id:uuidv1(), taskId: uuidv1(),type, data, status:JobStatus.init, runs:[],schedule:scheduleOption, retries:0, repeats:0},undefined);
        this.calcNextRun(job);
        await this.storeAdaptor.beforeSubmit(this, job);
        await this.queueAdaptor.beforeSubmit(this, job);
        job.status = JobStatus.waiting;
        await Bluebird.try(()=>this.storeAdaptor.save(job.jobInDb));
        await Bluebird.try(()=>this.queueAdaptor.sendTask(this,job));
        return job;
    }

    async reSubmit(job: JobWrap<D>): Promise<JobWrap<D>> {
        await this.storeAdaptor.beforeSubmit(this, job);
        await this.queueAdaptor.beforeSubmit(this, job);
        job.status = JobStatus.waiting;
        await Bluebird.try(()=>this.storeAdaptor.save(job.jobInDb));
        await Bluebird.try(()=>this.queueAdaptor.sendTask(this,job));
        return job;
    }

    calcNextRun(job: JobWrap<D>, lastRun?: Date){
        const scheduleOpt = job.schedule;
        //first run time calc
        if(job.status === JobStatus.init){
            if(scheduleOpt.after){
                job.nextRun = scheduleOpt.after;
            }else{
                job.nextRun = new Date();
            }
        }else if(job.status === JobStatus.completed && lastRun){
            job.nextRun = new Date(job.schedule.repeatOpt.wait + lastRun.getTime());
        }else{
            job.nextRun = null;
        }

    }

    cancel(jobId: JobId): void {
        throw new Error();
    }

    async find(jobId: JobId): Promise<JobIntf> {
        return this.storeAdaptor.findById(jobId);
    }

    listen(jobType: string, option: WorkerPoolOption, executor:Executor): void {
        console.log('listen');
        if (!!this.workerPoolMap.get(jobType)) {
            throw new Error('duplicated register executor');
        }
        const workerPool = new WorkerPool(option, executor, this.eventEmitter);
        console.log('set w p map', jobType, workerPool);
        this.workerPoolMap.set(jobType, workerPool);
    }

}